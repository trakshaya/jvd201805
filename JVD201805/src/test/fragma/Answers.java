package test.fragma;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import test.fragma.models.Deliveries;
import test.fragma.models.Matches;
import test.fragma.models.ResponseAnswer1;
import test.fragma.models.ResponseAnswer2;
import test.fragma.models.ResponseAnswer3;
import test.fragma.models.ResponseAnswer4;

public class Answers {

	private final ReadCSV readCSVObject = new ReadCSV();
	// ---------------- SOLUTION METHODS START -------------//

	// Question 1 : Top 4 teams which elected to field first after winning toss in
	// the year 2016 and 2017.
	// working to demonstrate Collection Set Iterations , Comparator etc
	protected void getAnswerOne() {
		List<Matches> matchesList = readCSVObject.readMatchesCSV();
		Iterator<Matches> itr = matchesList.iterator();
		Set<ResponseAnswer1> response16 = new HashSet<>();
		Set<ResponseAnswer1> response17 = new HashSet<>();
		while (itr.hasNext()) {
			Matches record = itr.next();
			if (record.getTossDecision().equals("field")) {
				if (record.getSeasonYear() == 2016) {
					response16 = this.getCountResponse(response16, record);
				} else if (record.getSeasonYear() == 2017) {
					response17 = this.getCountResponse(response17, record);
				}
			}
		}

		List<ResponseAnswer1> r16 = new ArrayList<ResponseAnswer1>(response16);
		List<ResponseAnswer1> r17 = new ArrayList<ResponseAnswer1>(response17);
		Collections.sort(r16, Comparator.comparing((ResponseAnswer1 o) -> o.getCount())
				.thenComparing((ResponseAnswer1 o) -> o.getTeam()).reversed());
		Collections.sort(r17, Comparator.comparing((ResponseAnswer1 o) -> o.getCount())
				.thenComparing((ResponseAnswer1 o) -> o.getTeam()).reversed());
		r16.stream().limit(4).forEach(answer16 -> System.out.println(answer16.toString()));
		r17.stream().limit(4).forEach(answer17 -> System.out.println(answer17.toString()));
	}

	// Question 2: List total number of fours, sixes, total score with respect to
	// team and year.
	// working to demonstrate MAP iterations and Collections
	protected void getAnswerTwo() {
		List<Deliveries> deliveriesList = readCSVObject.readDeliveriesCsv();
		Map<String, Object> tempDeliveryMap = new HashMap<>();
		List<Map<String, Object>> tempDelTrackList = new ArrayList<>();

		Iterator<Deliveries> itr = deliveriesList.iterator();
		int matchID = 0;
		int teamOneFourCount = 0;
		int teamTwoFourCount = 0;
		int teamOneSixCount = 0;
		int teamTwoSixCount = 0;
		int teamOneTotalRuns = 0;
		int teamTwoTotalRuns = 0;
		String teamOne = "";
		String teamTwo = "";
		while (itr.hasNext()) {
			Deliveries delRecord = itr.next();
			if ((matchID != 0 && matchID != delRecord.getMatchID()) || !itr.hasNext()) {
				tempDeliveryMap = new HashMap<>();
				tempDeliveryMap.put("matchID", matchID);
				tempDeliveryMap.put("teamOneFourCount", teamOneFourCount);
				tempDeliveryMap.put("teamTwoFourCount", teamTwoFourCount);
				tempDeliveryMap.put("teamOneSixCount", teamOneSixCount);
				tempDeliveryMap.put("teamTwoSixCount", teamTwoSixCount);
				tempDeliveryMap.put("teamOneTotalRuns", teamOneTotalRuns);
				tempDeliveryMap.put("teamTwoTotalRuns", teamTwoTotalRuns);
				tempDeliveryMap.put("teamOne", teamOne);
				tempDeliveryMap.put("teamTwo", teamTwo);
				tempDelTrackList.add(tempDeliveryMap);
				tempDeliveryMap = null;
				teamOneFourCount = 0;
				teamTwoFourCount = 0;
				teamOneSixCount = 0;
				teamTwoSixCount = 0;
				teamOneTotalRuns = 0;
				teamTwoTotalRuns = 0;
				teamOne = "";
				teamTwo = "";
			}
			matchID = delRecord.getMatchID();
			if (delRecord.getInning() == 1) {
				teamOne = delRecord.getBattingTeam();
				teamOneFourCount = teamOneFourCount + ((delRecord.getBatsmanRuns() == 4) ? 1 : 0);
				teamOneSixCount = teamOneSixCount + ((delRecord.getBatsmanRuns() == 6) ? 1 : 0);
				teamOneTotalRuns = teamOneTotalRuns + delRecord.getTotalRuns();
			} else if (delRecord.getInning() == 2) {
				teamTwo = delRecord.getBattingTeam();
				teamTwoFourCount = teamTwoFourCount + ((delRecord.getBatsmanRuns() == 4) ? 1 : 0);
				teamTwoSixCount = teamTwoSixCount + ((delRecord.getBatsmanRuns() == 6) ? 1 : 0);
				teamTwoTotalRuns = teamTwoTotalRuns + delRecord.getTotalRuns();
			}
		}

		tempDelTrackList = this.addYearToTempDeliveryList(tempDelTrackList);

		Set<ResponseAnswer2> response = new HashSet<>();
		Iterator<Map<String, Object>> itr2 = tempDelTrackList.iterator();
		while (itr2.hasNext()) {
			Map<String, Object> tempTrackRecord = itr2.next();
			response = this.getResponseAnswer2(response, tempTrackRecord);
		}

		List<ResponseAnswer2> rTemp = new ArrayList<ResponseAnswer2>(response);
		Collections.sort(rTemp, Comparator.comparing((ResponseAnswer2 o) -> o.getYear())
				.thenComparing((ResponseAnswer2 o) -> o.getTeam()));
		rTemp.stream().forEach(obj -> System.out.println(obj.toString()));
	}

	// Question3 :Top 10 best economy rate bowler with respect to year who bowled at
	// least 10
	// overs ( LEGBYE_RUNS and BYE_RUNS should not be considered for Total Runs
	// Given
	// by a bowler)
	protected void getAnswerThree() {
		Set<ResponseAnswer3> response = new HashSet<>();
		List<Matches> matchesList = readCSVObject.readMatchesCSV();
		List<Deliveries> deliveriesList = readCSVObject.readDeliveriesCsv();

		Set<String> bowlersSet = new HashSet<>();
		// make bowler set using enhanced for loop
		for (Deliveries delivery : deliveriesList) {
			bowlersSet.add(delivery.getBowler());
		}

		Set<Integer> yearSet = new HashSet<>();
		// make a delivery list for each bowler
		for (String bowler : bowlersSet) {
			List<Deliveries> bowlerDeliveryList = deliveriesList.stream()
					.filter(obj -> (obj.getBowler().equals(bowler))).collect(Collectors.toList());

			float overCount = this.getOversCount(bowlerDeliveryList);
			if (overCount >= 10) {
				Set<Integer> matchIDSet = bowlerDeliveryList.stream().map(obj -> obj.getMatchID()).distinct()
						.collect(Collectors.toSet());
				Iterator<Integer> itr = matchIDSet.iterator();
				while (itr.hasNext()) {
					int matchID = itr.next();
					int year = matchesList.stream().filter(obj -> (obj.getMatchID() == matchID))
							.map(m -> m.getSeasonYear()).findFirst().get();
					yearSet.add(year);
					response = this.getResponseAnswer3(response, bowlerDeliveryList, matchID, year);
				}
			}
		}

		List<ResponseAnswer3> tempLResponseList = new ArrayList<>(response);
		Collections.sort(tempLResponseList, Comparator.comparing((ResponseAnswer3 o) -> o.getYear())
				.thenComparing((ResponseAnswer3 o) -> o.getEconomy()));
		List<ResponseAnswer3> finalResponse = new ArrayList<>();
		for (Integer year : yearSet) {
			List<ResponseAnswer3> yearListResponse = tempLResponseList.stream().filter(obj -> (obj.getYear() == year))
					.limit(10).collect(Collectors.toList());
			finalResponse.addAll(yearListResponse);
			yearListResponse = null;
		}
		Collections.sort(finalResponse, Comparator.comparing((ResponseAnswer3 o) -> o.getYear()));
		finalResponse.stream().forEach(obj -> System.out.println(obj.toString()));
	}

	// Question 4 : Find the team name which has Highest Net Run Rate with respect
	// to year.
	// Net Run Rate = (Total Runs Scored / Total Overs Faced) � (Total Runs
	// Conceded /Total Overs Bowled)
	protected void getAnswerFour() {
		Set<ResponseAnswer4> response = new HashSet<>();
		List<Matches> matchesList = readCSVObject.readMatchesCSV();
		List<Deliveries> deliveriesList = readCSVObject.readDeliveriesCsv();
		Set<Integer> matchIDBattingSet = deliveriesList.stream().map(m -> m.getMatchID()).distinct()
				.collect(Collectors.toSet()); // if matchId's are given other than sequential order of matchesList
		Iterator<Integer> matchIDitr = matchIDBattingSet.iterator();
		Set<Integer> yearSet = new HashSet<>();
		while (matchIDitr.hasNext()) {
			int matchID = matchIDitr.next();
			int year = matchesList.stream().filter(obj -> (obj.getMatchID() == matchID)).map(m -> m.getSeasonYear())
					.findFirst().get();
			yearSet.add(year);
			response = this.getResponseAnswer4(response, deliveriesList, matchID, year);
		}

		List<ResponseAnswer4> tempResponseList = new ArrayList<>(response);
		Collections.sort(tempResponseList, Comparator.comparing((ResponseAnswer4 o) -> o.getYear())
				.thenComparing((ResponseAnswer4 o) -> o.getNetRunRate()).reversed());
		List<ResponseAnswer4> finalResponse = new ArrayList<>();
		for (Integer year : yearSet) {
			ResponseAnswer4 yearResponse = tempResponseList.stream()
					.filter(obj -> ((obj.getYear() == year) && (!Double.isNaN(obj.getNetRunRate())))).findFirst().get();
			finalResponse.add(yearResponse);
		}
		Collections.sort(finalResponse, Comparator.comparing((ResponseAnswer4 o) -> o.getYear()));
		finalResponse.stream().forEach(obj -> System.out.println(obj.toString()));
	}

	// -------------------------- SOLUTION METHODS END --------------------------//

	// ------------------ UTILITY PRIVATE METHODS --------------------------//
	private ResponseAnswer1 getEqualResponse(Set<ResponseAnswer1> response, String responseTeam, int responseYear) {
		Iterator<ResponseAnswer1> itr = response.iterator();
		while (itr.hasNext()) {
			ResponseAnswer1 record = itr.next();
			if (record.getTeam().equals(responseTeam) && (record.getYear() == responseYear)) {
				return record;
			}
		}
		return null;
	}

	private Set<ResponseAnswer1> getCountResponse(Set<ResponseAnswer1> response, Matches record) {
		ResponseAnswer1 responseRecord = this.getEqualResponse(response, record.getTossWinner(),
				record.getSeasonYear());
		if (responseRecord != null) {
			responseRecord.setCount(responseRecord.getCount() + 1);
		} else {
			responseRecord = new ResponseAnswer1();
			responseRecord.setTeam(record.getTossWinner());
			responseRecord.setCount(1);
			responseRecord.setYear(record.getSeasonYear());
		}
		response.remove(responseRecord);
		response.add(responseRecord);
		return response;
	}

	//
	private ResponseAnswer2 getEqualResponse2(Set<ResponseAnswer2> response, int year, String teamName) {
		Iterator<ResponseAnswer2> itr = response.iterator();
		while (itr.hasNext()) {
			ResponseAnswer2 record = itr.next();
			if ((record.getTeam().equals(teamName)) && (record.getYear() == year)) {
				return record;
			}
		}
		return null;
	}

	private Set<ResponseAnswer2> getResponseAnswer2(Set<ResponseAnswer2> response, Map<String, Object> record) {
		int year = Integer.parseInt(String.valueOf(record.get("year")));
		String teamName = String.valueOf(record.get("teamOne"));

		ResponseAnswer2 responseTeamOneFound = this.getEqualResponse2(response, year, teamName);
		int fourRunsCount = Integer.parseInt(String.valueOf(record.get("teamOneFourCount")));
		int sixRunsCount = Integer.parseInt(String.valueOf(record.get("teamOneSixCount")));
		int totalRunsCount = Integer.parseInt(String.valueOf(record.get("teamOneTotalRuns")));

		if (responseTeamOneFound != null) {
			responseTeamOneFound.setFours(responseTeamOneFound.getFours() + fourRunsCount);
			responseTeamOneFound.setSixes(responseTeamOneFound.getSixes() + sixRunsCount);
			responseTeamOneFound.setTotalRuns(responseTeamOneFound.getTotalRuns() + totalRunsCount);
		} else {
			responseTeamOneFound = new ResponseAnswer2();
			responseTeamOneFound.setFours(fourRunsCount);
			responseTeamOneFound.setSixes(sixRunsCount);
			responseTeamOneFound.setTeam(teamName);
			responseTeamOneFound.setTotalRuns(totalRunsCount);
			responseTeamOneFound.setYear(year);
		}
		response.remove(responseTeamOneFound);
		response.add(responseTeamOneFound);

		teamName = String.valueOf(record.get("teamTwo"));
		ResponseAnswer2 responseTeamTwoFound = this.getEqualResponse2(response, year, teamName);
		fourRunsCount = Integer.parseInt(String.valueOf(record.get("teamTwoFourCount")));
		sixRunsCount = Integer.parseInt(String.valueOf(record.get("teamTwoSixCount")));
		totalRunsCount = Integer.parseInt(String.valueOf(record.get("teamTwoTotalRuns")));
		if (responseTeamTwoFound != null) {
			responseTeamTwoFound.setFours(responseTeamTwoFound.getFours() + fourRunsCount);
			responseTeamTwoFound.setSixes(responseTeamTwoFound.getSixes() + sixRunsCount);
			responseTeamTwoFound.setTotalRuns(responseTeamTwoFound.getTotalRuns() + totalRunsCount);
		} else {
			responseTeamTwoFound = new ResponseAnswer2();
			responseTeamTwoFound.setFours(fourRunsCount);
			responseTeamTwoFound.setSixes(sixRunsCount);
			responseTeamTwoFound.setTeam(teamName);
			responseTeamTwoFound.setTotalRuns(totalRunsCount);
			responseTeamTwoFound.setYear(year);
		}
		response.remove(responseTeamTwoFound);
		response.add(responseTeamTwoFound);
		return response;
	}

	private List<Map<String, Object>> addYearToTempDeliveryList(List<Map<String, Object>> tempDelTrackList) {
		// add year to the tempDelTrackTList on basis of match ID
		ListIterator<Map<String, Object>> ltr = tempDelTrackList.listIterator();
		List<Matches> matchesList = readCSVObject.readMatchesCSV();
		while (ltr.hasNext()) {
			Map<String, Object> tempTrackRecord = ltr.next();
			int matchId = Integer.parseInt(String.valueOf(tempTrackRecord.get("matchID")));
			int seasonYear = matchesList.stream().filter(match -> (match.getMatchID() == matchId)).findAny()
					.map(v -> v.getSeasonYear()).orElse(0);
			tempTrackRecord.put("year", seasonYear);

		}
		return tempDelTrackList;
	}

	//
	//
	private Set<ResponseAnswer3> getResponseAnswer3(Set<ResponseAnswer3> response, List<Deliveries> bowlerDeliveryList,
			int matchID, int year) {
		String player = bowlerDeliveryList.stream().filter(obj -> (obj.getMatchID() == matchID)).map(m -> m.getBowler())
				.findFirst().get();

		List<Deliveries> matchIDBowlerDeliveriesList = bowlerDeliveryList.stream()
				.filter(obj -> (obj.getMatchID() == matchID)).collect(Collectors.toList());
		Iterator<Deliveries> itr = matchIDBowlerDeliveriesList.iterator();
		float totalRuns = 0;
		float byRuns = 0;
		float legByRuns = 0;
		float economy = 0;
		float oversBowled = this.getOversCount(matchIDBowlerDeliveriesList); // overs bowled can be in float where a
																			// over etc.
		while (itr.hasNext()) {
			Deliveries delivery = itr.next();
			totalRuns = totalRuns + delivery.getTotalRuns();
			byRuns = byRuns + delivery.getByeRuns();
			legByRuns = legByRuns + delivery.getLegByeRuns();
		}
		ResponseAnswer3 responseRecord = this.getEqualResponse3(response, year, player);
		if (responseRecord != null) {
			totalRuns = totalRuns + responseRecord.getTotalRuns();
			byRuns = byRuns + responseRecord.getByeRuns();
			legByRuns = legByRuns + responseRecord.getLegByeRuns();
			oversBowled = oversBowled + responseRecord.getOverBowled();
			economy = (totalRuns - (byRuns + legByRuns)) / oversBowled;
			responseRecord.setTotalRuns(totalRuns);
			responseRecord.setByeRuns(byRuns);
			responseRecord.setEconomy(economy);
			responseRecord.setLegByeRuns(legByRuns);
			responseRecord.setOverBowled(oversBowled);
			responseRecord.setPlayer(player);
			responseRecord.setYear(year);
		} else {
			responseRecord = new ResponseAnswer3();
			economy = (totalRuns - (byRuns + legByRuns)) / oversBowled;
			responseRecord.setTotalRuns(totalRuns);
			responseRecord.setByeRuns(byRuns);
			responseRecord.setEconomy(economy);
			responseRecord.setLegByeRuns(legByRuns);
			responseRecord.setOverBowled(oversBowled);
			responseRecord.setPlayer(player);
			responseRecord.setYear(year);
		}
		response.remove(responseRecord);
		response.add(responseRecord);
		return response;
	}

	//
	private ResponseAnswer3 getEqualResponse3(Set<ResponseAnswer3> response, int year, String player) {
		Iterator<ResponseAnswer3> itr = response.iterator();
		while (itr.hasNext()) {
			ResponseAnswer3 record = itr.next();
			if ((record.getPlayer().equals(player)) && (record.getYear() == year)) {
				return record;
			}
		}
		return null;
	}

	private Set<ResponseAnswer4> getResponseAnswer4(Set<ResponseAnswer4> response, List<Deliveries> deliveriesList,
			int matchID, int year) {
		List<Deliveries> matchIDList = deliveriesList.stream().filter(obj -> (obj.getMatchID() == matchID))
				.collect(Collectors.toList());
		List<Deliveries> firstInningsList = matchIDList.stream().filter(obj -> (obj.getInning() == 1))
				.collect(Collectors.toList());
		List<Deliveries> secondInningsList = matchIDList.stream().filter(obj -> (obj.getInning() == 2))
				.collect(Collectors.toList());

		float teamOneRunsScored = 0;
		float teamTwoRunsScored = 0;
		float teamOneRunsConceaded = 0;
		float teamTwoRunsConceaded = 0;
		float teamOneOverFaced = 0;
		float teamTwoOverFaced = 0;
		float teamOneOverBowled = 0;
		float teamTwoOverBowled = 0;
		float teamOneNetRunRate = 0;
		float teamTwoNetRunRate = 0;
		String teamOne = "";
		String teamTwo = "";

		Iterator<Deliveries> firstItr = firstInningsList.iterator();
		while (firstItr.hasNext()) {
			Deliveries delivery = firstItr.next();
			teamOne = delivery.getBattingTeam(); // batting team : team one
			teamOneRunsScored = teamOneRunsScored + delivery.getTotalRuns();
			teamTwoRunsConceaded = teamTwoRunsConceaded + delivery.getTotalRuns();

		}
		teamOneOverFaced = this.getOversCount(firstInningsList); // Calculation for over can be greater than 20 as
																// deliveries bowled can be greater than 120;
		teamTwoOverBowled = teamOneOverFaced;

		Iterator<Deliveries> secondItr = secondInningsList.iterator();
		while (secondItr.hasNext()) {
			Deliveries delivery = secondItr.next();
			teamTwo = delivery.getBattingTeam(); // batting team : team one
			teamTwoRunsScored = teamTwoRunsScored + delivery.getTotalRuns();
			teamOneRunsConceaded = teamOneRunsConceaded + delivery.getTotalRuns();
		}
		teamTwoOverFaced = this.getOversCount(secondInningsList);
		teamOneOverBowled = teamTwoOverFaced;

		ResponseAnswer4 teamOneResponseRecord = this.getEqualResponse4(response, year, teamOne);
		if (teamOneResponseRecord != null) {
			teamOneOverBowled = teamOneOverBowled + teamOneResponseRecord.getTotalOversBowled();
			teamOneOverFaced = teamOneOverFaced + teamOneResponseRecord.getTotalOversFaced();
			teamOneRunsConceaded = teamOneRunsConceaded + teamOneResponseRecord.getTotalRunsConceaded();
			teamOneRunsScored = teamOneRunsScored + teamOneResponseRecord.getTotalRunsScored();
			teamOneNetRunRate = ((float) (teamOneRunsScored / teamOneOverFaced)
					- (teamOneRunsConceaded / teamOneOverBowled));

			teamOneResponseRecord.setTeam(teamOne);
			teamOneResponseRecord.setTotalOversBowled(teamOneOverBowled);
			teamOneResponseRecord.setTotalOversFaced(teamOneOverFaced);
			teamOneResponseRecord.setTotalRunsConceaded(teamOneRunsConceaded);
			teamOneResponseRecord.setTotalRunsScored(teamOneRunsScored);
			teamOneResponseRecord.setYear(year);
			teamOneResponseRecord.setNetRunRate(teamOneNetRunRate);
		} else {
			teamOneResponseRecord = new ResponseAnswer4();
			teamOneNetRunRate = ((float) (teamOneRunsScored / teamOneOverFaced)
					- (teamOneRunsConceaded / teamOneOverBowled));

			teamOneResponseRecord.setTeam(teamOne);
			teamOneResponseRecord.setTotalOversBowled(teamOneOverBowled);
			teamOneResponseRecord.setTotalOversFaced(teamOneOverFaced);
			teamOneResponseRecord.setTotalRunsConceaded(teamOneRunsConceaded);
			teamOneResponseRecord.setTotalRunsScored(teamOneRunsScored);
			teamOneResponseRecord.setYear(year);
			teamOneResponseRecord.setNetRunRate(teamOneNetRunRate);
		}
		response.remove(teamOneResponseRecord);
		response.add(teamOneResponseRecord);
		ResponseAnswer4 teamTwoResponseRecord = this.getEqualResponse4(response, year, teamTwo);
		if (teamTwoResponseRecord != null) {
			teamTwoOverBowled = teamTwoOverBowled + teamTwoResponseRecord.getTotalOversBowled();
			teamTwoOverFaced = teamTwoOverFaced + teamTwoResponseRecord.getTotalOversFaced();
			teamTwoRunsConceaded = teamTwoRunsConceaded + teamTwoResponseRecord.getTotalRunsConceaded();
			teamTwoRunsScored = teamTwoRunsScored + teamTwoResponseRecord.getTotalRunsScored();
			teamTwoNetRunRate = ((float) (teamTwoRunsScored / teamTwoOverFaced)
					- (teamTwoRunsConceaded / teamTwoOverBowled));

			teamTwoResponseRecord.setTeam(teamTwo);
			teamTwoResponseRecord.setTotalOversBowled(teamTwoOverBowled);
			teamTwoResponseRecord.setTotalOversFaced(teamTwoOverFaced);
			teamTwoResponseRecord.setTotalRunsConceaded(teamTwoRunsConceaded);
			teamTwoResponseRecord.setTotalRunsScored(teamTwoRunsScored);
			teamTwoResponseRecord.setYear(year);
			teamTwoResponseRecord.setNetRunRate(teamTwoNetRunRate);
		} else {
			teamTwoResponseRecord = new ResponseAnswer4();
			teamTwoNetRunRate = ((float) (teamTwoRunsScored / teamTwoOverFaced)
					- (teamTwoRunsConceaded / teamTwoOverBowled));

			teamTwoResponseRecord.setTeam(teamTwo);
			teamTwoResponseRecord.setTotalOversBowled(teamTwoOverBowled);
			teamTwoResponseRecord.setTotalOversFaced(teamTwoOverFaced);
			teamTwoResponseRecord.setTotalRunsConceaded(teamTwoRunsConceaded);
			teamTwoResponseRecord.setTotalRunsScored(teamTwoRunsScored);
			teamTwoResponseRecord.setYear(year);
			teamTwoResponseRecord.setNetRunRate(teamTwoNetRunRate);
		}

		response.remove(teamTwoResponseRecord);
		response.add(teamTwoResponseRecord);
		return response;
	}

	private ResponseAnswer4 getEqualResponse4(Set<ResponseAnswer4> response, int year, String team) {
		Iterator<ResponseAnswer4> itr = response.iterator();
		while (itr.hasNext()) {
			ResponseAnswer4 record = itr.next();
			if ((record.getTeam().equals(team)) && (record.getYear() == year)) {
				return record;
			}
		}
		return null;

	}

	// getOversCount
	private float getOversCount(List<Deliveries> deliveryList) {
		float overCount = 0;
		Set<Integer> matchIDSet = deliveryList.stream().map(obj -> obj.getMatchID()).sorted()
				.collect(Collectors.toSet());
		Iterator<Integer> itr = matchIDSet.iterator();
		while (itr.hasNext()) {
			int matchId = itr.next();
			List<Deliveries> matchIdDeliveryList = deliveryList.stream().filter(obj -> (obj.getMatchID() == matchId))
					.collect(Collectors.toList());
			ListIterator<Deliveries> ltr = matchIdDeliveryList.listIterator();
			while (ltr.hasNext()) {
				Deliveries delivery = ltr.next();
				int overValue = delivery.getOver();
				int ballValue = delivery.getBall();
				int nextOverValue = 0;
				if (!ltr.hasNext()) {
					overCount = overCount + ((ballValue >= 6) ? 1 : (ballValue / 10));
				} else {
					nextOverValue = matchIdDeliveryList.get(ltr.nextIndex()).getOver();
					if (overValue != nextOverValue) {
						overCount++;
					}
				}
			}
		}
		return overCount;
	}
	// --------------------- UTILITY PRIVATE METHODS END ------------------//
}
