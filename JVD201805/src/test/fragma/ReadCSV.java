package test.fragma;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import test.fragma.models.Deliveries;
import test.fragma.models.Matches;

public class ReadCSV {

	/*public static void main(String[] args) {
		ReadCSV o = new ReadCSV();
		o.readDeliveriesCsv();
		o.readMatchesCSV();
	}*/
	
	// Method : Read and return Deliveries List
	protected List<Deliveries> readDeliveriesCsv() {
		List<Deliveries> deliveriesList = new ArrayList<>();
		try {
			InputStream inputStream = getClass().getClassLoader().getResourceAsStream("deliveries.csv");
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			String line = reader.readLine();
			// MATCH_ID,INNING,BATTING_TEAM,BOWLING_TEAM,OVER,BALL,BATSMAN,BOWLER,
			// WIDE_RUNS,BYE_RUNS,LEGBYE_RUNS,NOBALL_RUNS,PENALTY_RUNS,BATSMAN_RUNS,EXTRA_RUNS,TOTAL_RUNS
			if (line != null) {
				while ((line = reader.readLine()) != null) {
					List<String> record = Arrays.asList(line.split(","));
					Deliveries deliveries = new Deliveries(record.get(0), record.get(1), record.get(2), record.get(3),
							record.get(4), record.get(5), record.get(6), record.get(7), record.get(8), record.get(9),
							record.get(10), record.get(11), record.get(12), record.get(13), record.get(14),
							record.get(15));
					deliveriesList.add(deliveries);
				}
			}
		} catch (IOException ioException) {
			ioException.printStackTrace();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return deliveriesList;
	}

	// Method : Read and return Matches List
	protected List<Matches> readMatchesCSV() {
		List<Matches> matchesList = new ArrayList<>();

		try {
			InputStream inputStream = getClass().getClassLoader().getResourceAsStream("matches.csv");
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			String line = reader.readLine();
			// MATCH_ID,SEASON,CITY,DATE,TEAM1,TEAM2,TOSS_WINNER,TOSS_DECISION,RESULT,WINNER
			if (line != null) {
				while ((line = reader.readLine()) != null) {
					List<String> record = Arrays.asList(line.split(","));
					Matches matches = new Matches(record.get(0), record.get(1), record.get(2), record.get(3),
							record.get(4), record.get(5), record.get(6), record.get(7), record.get(8),
							(record.get(8).equals("no result") ? null : record.get(9)));
					matchesList.add(matches);
				}
			}
		} catch (IOException ioException) {
			ioException.printStackTrace();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return matchesList;
	}
}
