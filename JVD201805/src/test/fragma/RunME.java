package test.fragma;

import java.util.InputMismatchException;
import java.util.Scanner;

public class RunME {

	public static void main(String[] args) {

		Answers answers = new Answers();

		Scanner reader = new Scanner(System.in); // Reading from System.in
		System.out.println("Enter the Question number of which you want answer , To exit press any other digit. ");

		Object input ;
		int inputQuestionNumber = 0;
		do {
			System.out.println(
					"-----------------------------------------------------------------------------------");
			System.out.print("Enter Value : ");
			try{
				input = reader.nextInt();
				inputQuestionNumber = Integer.parseInt(String.valueOf(input));
				switch (inputQuestionNumber) {
				case 1:
					System.out.println("Note* : ");
					System.out.println("1.The years mentioned in the question can produce ambiguous result \n"
							+ "with combined data for year 2016 and 2017. \n "
							+ "Thus, the following result is divided on bases of year 2016 and 2017 ."
							+ "\n Hopefully that is what was required.");
					System.out.println(
							"-----------------------------------------------------------------------------------");
					answers.getAnswerOne();
					break;
				case 2:
					System.out.println("1.Different , Classic and basic Map approach has been used just to demonstrate.");
					System.out.println(
							"-----------------------------------------------------------------------------------");
					answers.getAnswerTwo();
					break;
				case 3:
					System.out.println(
							"-----------------------------------------------------------------------------------");
					answers.getAnswerThree();
					break;
				case 4:
					System.out.println(
							"-----------------------------------------------------------------------------------");
					answers.getAnswerFour();
					break;
				default:
					System.out.println("Invalid Value Entered. Run Again");
					break;
				}
			}catch (InputMismatchException ex){
				System.out.println("Invalid Value Entered. Run Again");
			}
		} while (inputQuestionNumber == 1 || inputQuestionNumber == 2 || inputQuestionNumber == 3
				|| inputQuestionNumber == 4);
		
		reader.close();
	}

}
