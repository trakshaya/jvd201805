package test.fragma.models;

public class Matches {
	private int matchID;
	private int seasonYear;
	private String city;
	private String date;
	private String team1;
	private String team2;
	private String tossWinner;
	private String tossDecision;
	private String result;
	private String winner;
	
	//custom Constructor
	public Matches(String matchID, String seasonYear, String city, String date, String team1, String team2, String tossWinner,
			String tossDecision, String result, String winner) {
		super();
		this.matchID = Integer.parseInt(matchID);
		this.seasonYear = Integer.parseInt(seasonYear);
		this.city = city;
		this.date = date;
		this.team1 = team1;
		this.team2 = team2;
		this.tossWinner = tossWinner;
		this.tossDecision = tossDecision;
		this.result = result;
		this.winner = winner;
	}

	//getters
	public int getMatchID() {
		return matchID;
	}

	public int getSeasonYear() {
		return seasonYear;
	}

	public String getCity() {
		return city;
	}

	public String getDate() {
		return date;
	}

	public String getTeam1() {
		return team1;
	}

	public String getTeam2() {
		return team2;
	}

	public String getTossWinner() {
		return tossWinner;
	}

	public String getTossDecision() {
		return tossDecision;
	}

	public String getResult() {
		return result;
	}

	public String getWinner() {
		return winner;
	}

	@Override
	public String toString() {
		return "Matches [matchID=" + matchID + ", seasonYear=" + seasonYear + ", city=" + city + ", date=" + date
				+ ", team1=" + team1 + ", team2=" + team2 + ", tossWinner=" + tossWinner + ", tossDecision="
				+ tossDecision + ", result=" + result + ", winner=" + winner + "]";
	}
	
}
