package test.fragma.models;

public class ResponseAnswer4 {

	int year;
	String team;
	float totalRunsScored;
	float totalOversFaced;
	float totalRunsConceaded;
	float totalOversBowled;
	float netRunRate;
	
	
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	public float getTotalRunsScored() {
		return totalRunsScored;
	}
	public void setTotalRunsScored(float totalRunsScored) {
		this.totalRunsScored = totalRunsScored;
	}
	public float getTotalOversFaced() {
		return totalOversFaced;
	}
	public void setTotalOversFaced(float totalOversFaced) {
		this.totalOversFaced = totalOversFaced;
	}
	public float getTotalRunsConceaded() {
		return totalRunsConceaded;
	}
	public void setTotalRunsConceaded(float totalRunsConceaded) {
		this.totalRunsConceaded = totalRunsConceaded;
	}
	public float getTotalOversBowled() {
		return totalOversBowled;
	}
	public void setTotalOversBowled(float totalOversBowled) {
		this.totalOversBowled = totalOversBowled;
	}
	public float getNetRunRate() {
		return netRunRate;
	}
	public void setNetRunRate(float netRunRate) {
		this.netRunRate = netRunRate;
	}
	
	@Override
	public String toString() {
		return "ResponseAnswer4 [year : "+ year +" | netRunRate : " + String.format("%.03f", netRunRate) + " | team : " + team + "] ";
	}
	
	
}
