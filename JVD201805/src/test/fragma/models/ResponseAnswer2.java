package test.fragma.models;

public class ResponseAnswer2 {

	private String team;
	private int year;
	private int fours;
	private int sixes;
	private int totalRuns;
	
	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getFours() {
		return fours;
	}
	public void setFours(int fours) {
		this.fours = fours;
	}
	public int getSixes() {
		return sixes;
	}
	public void setSixes(int sixes) {
		this.sixes = sixes;
	}
	public int getTotalRuns() {
		return totalRuns;
	}
	public void setTotalRuns(int totalRuns) {
		this.totalRuns = totalRuns;
	}
	@Override
	public String toString() {
		return "Answer2 Response: [team : " + team + " | year : " + year + " | fours : " + fours
				+ " | sixes : " + sixes + " | totalRuns : " + totalRuns + "]";
	}
	
	
}
