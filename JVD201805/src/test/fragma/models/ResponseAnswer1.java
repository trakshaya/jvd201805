package test.fragma.models;

public class ResponseAnswer1 {

	private int year;
	private String team;
	private int count = 0;

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "Answer1 Response :  [year : " + year + " | count : " + count + " | team : " + team + "]";
	}

	
}
