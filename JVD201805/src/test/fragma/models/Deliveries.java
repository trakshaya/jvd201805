package test.fragma.models;

public class Deliveries {

	private int matchID;
	private int inning;
	private String battingTeam;
	private String bowlingTeam;
	private int over;
	private int ball;
	private String batsman;
	private String bowler;
	private int wideRuns;
	private int byeRuns;
	private int legByeRuns;
	private int noBallRuns;
	private int penaltyRuns;
	private int batsmanRuns;
	private int extraRuns;
	private int totalRuns;

	//custom Constructor
	public Deliveries(String matchID, String inning, String battingTeam, String bowlingTeam, String over, String ball,
			String batsman, String bowler, String wideRuns, String byeRuns, String legByeRuns, String noBallRuns, String penaltyRuns,
			String batsmanRuns, String extraRuns, String totalRuns) {
		super();
		this.matchID = Integer.parseInt(matchID);
		this.inning = Integer.parseInt(inning);
		this.battingTeam = battingTeam;
		this.bowlingTeam = bowlingTeam;
		this.over = Integer.parseInt(over);
		this.ball = Integer.parseInt(ball);
		this.batsman = batsman;
		this.bowler = bowler;
		this.wideRuns = Integer.parseInt(wideRuns);
		this.byeRuns = Integer.parseInt(byeRuns);
		this.legByeRuns = Integer.parseInt(legByeRuns);
		this.noBallRuns = Integer.parseInt(noBallRuns);
		this.penaltyRuns = Integer.parseInt(penaltyRuns);
		this.batsmanRuns = Integer.parseInt(batsmanRuns);
		this.extraRuns = Integer.parseInt(extraRuns);
		this.totalRuns = Integer.parseInt(totalRuns);
	}

	// getters 
	public int getMatchID() {
		return matchID;
	}

	public int getInning() {
		return inning;
	}

	public String getBattingTeam() {
		return battingTeam;
	}

	public String getBowlingTeam() {
		return bowlingTeam;
	}

	public int getOver() {
		return over;
	}

	public int getBall() {
		return ball;
	}

	public String getBatsman() {
		return batsman;
	}

	public String getBowler() {
		return bowler;
	}

	public int getWideRuns() {
		return wideRuns;
	}

	public int getByeRuns() {
		return byeRuns;
	}

	public int getLegByeRuns() {
		return legByeRuns;
	}

	public int getNoBallRuns() {
		return noBallRuns;
	}

	public int getPenaltyRuns() {
		return penaltyRuns;
	}

	public int getBatsmanRuns() {
		return batsmanRuns;
	}

	public int getExtraRuns() {
		return extraRuns;
	}

	public int getTotalRuns() {
		return totalRuns;
	}

	@Override
	public String toString() {
		return "Deliveries [matchID=" + matchID + ", inning=" + inning + ", battingTeam=" + battingTeam
				+ ", bowlingTeam=" + bowlingTeam + ", over=" + over + ", ball=" + ball + ", batsman=" + batsman
				+ ", bowler=" + bowler + ", wideRuns=" + wideRuns + ", byeRuns=" + byeRuns + ", legByeRuns="
				+ legByeRuns + ", noBallRuns=" + noBallRuns + ", penaltyRuns=" + penaltyRuns + ", batsmanRuns="
				+ batsmanRuns + ", extraRuns=" + extraRuns + ", totalRuns=" + totalRuns + "]";
	}

	
}
