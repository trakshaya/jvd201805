package test.fragma.models;

public class ResponseAnswer3 {

	private int year;
	private String player;
	private float economy;
	private float totalRuns;
	private float byeRuns;
	private float legByeRuns;
	private float overBowled;
	
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getPlayer() {
		return player;
	}
	public void setPlayer(String player) {
		this.player = player;
	}
	public float getEconomy() {
		return economy;
	}
	public void setEconomy(float economy) {
		this.economy = economy;
	}
	public float getTotalRuns() {
		return totalRuns;
	}
	public void setTotalRuns(float totalRuns2) {
		this.totalRuns = totalRuns2;
	}
	public float getByeRuns() {
		return byeRuns;
	}
	public void setByeRuns(float byRuns) {
		this.byeRuns = byRuns;
	}
	public float getLegByeRuns() {
		return legByeRuns;
	}
	public void setLegByeRuns(float legByRuns) {
		this.legByeRuns = legByRuns;
	}
	

	public float getOverBowled() {
		return overBowled;
	}
	public void setOverBowled(float oversBowled) {
		this.overBowled = oversBowled;
	}
	@Override
	public String toString() {
		return "ResponseAnswer3 [year : "+ year +" | economy=" + String.format("%.02f", economy) + " | player : " + player + "]";
	}
	
}
